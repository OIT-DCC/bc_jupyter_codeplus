# bc_jupyter_codeplus

Open OnDemand Batch Connect application that will run Jupyterlab from a Singularity container. This is in support of the Summer 2022 CodePlus projects.
